package io.rickenbazolo.reactiveapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringReactiveRestfulApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringReactiveRestfulApiApplication.class, args);
	}

}
