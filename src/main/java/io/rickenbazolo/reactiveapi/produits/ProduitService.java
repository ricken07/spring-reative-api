/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.rickenbazolo.reactiveapi.produits;

import io.rickenbazolo.reactiveapi.produits.events.CreationProduit;
import io.rickenbazolo.reactiveapi.produits.events.processus.CreationProduitEventProcessor;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

/**
 *
 * @author Ricken BAZOLO
 */
@Slf4j
@Service
public class ProduitService {
    private final CategoriRepository categoriRepository;
    private final ProduitRepository produitRepository;
    private final ApplicationEventPublisher eventPublisher;

    @Autowired
    public ProduitService(CategoriRepository categoriRepository, 
            ProduitRepository produitRepository, 
            ApplicationEventPublisher eventPublisher) {
        this.categoriRepository = categoriRepository;
        this.produitRepository = produitRepository;
        this.eventPublisher = eventPublisher;
    }
    
    public Produits creerProduits(Produits produit){
        log.debug("Cration d'un produit", "Titre: "+produit.getTitle());
        
        Assert.notNull(produit.getCategorie(), "La catégorie est null!");
        
        final Categorie categorie = categoriRepository.getOne(produit.getCategorie().getId());
        //produit.setCategorie(categorie);
        Produits save = produitRepository.save(produit);
        
        log.debug("Le produit créé", "ID: "+save.getId(), "Titre: "+save.getTitle());
        
        this.eventPublisher.publishEvent(new CreationProduit(save));
        
        return save;
    }
    
    public List<Produits> allProduits(){
        return this.produitRepository.findAll();
    }
    
}
