/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.rickenbazolo.reactiveapi.produits;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 *
 * @author Ricken BAZOLO
 */
@Slf4j
@Component
public class MyRunner implements CommandLineRunner{

    @Autowired
    private CategoriRepository repository;
    
    @Override
    public void run(String... args) throws Exception {
        repository.deleteAll();
        List<Categorie> streams = Arrays.asList(
                new Categorie(1, "IMMOBILIER"),
                new Categorie(2, "VETEMENT"),
                new Categorie(3, "ELECTRONIQUE"),
                new Categorie(4, "CHAUSSURE")
        );
        
        streams.forEach((s) -> {
            repository.save(s);
            log.debug("Categorie**", "Cat: "+ s.getTitle());
        });
        repository.save(new Categorie(5, "IMMOBILIER") );
        log.info("categorie sauvegarder");
    }
    
}
