/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.rickenbazolo.reactiveapi.produits;

import io.rickenbazolo.reactiveapi.produits.events.CreationProduit;
import io.rickenbazolo.reactiveapi.produits.events.processus.CreationProduitEventProcessor;
import java.net.URI;
import java.util.List;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

/**
 *
 * @author Ricken BAZOLO
 */
@RestController
@Slf4j
public class ProduitsController {
    
    private final ProduitService produitService;
    private final Flux<CreationProduit> events;

    public ProduitsController(ProduitService produitService,
            CreationProduitEventProcessor executor) {
        this.produitService = produitService;
        this.events = Flux.create(executor).share();
    }
    
    @CrossOrigin
    @PostMapping("/produit")
    ResponseEntity<Long> addCourse(@RequestBody @Valid Produits produit){
        try{
            Produits p = this.produitService.creerProduits(produit);
            return ResponseEntity.created(URI.create("/produit/" + p.getId())).body(p.getId());
        }catch(Exception e){
            log.error(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    @CrossOrigin()
    @GetMapping(value = "/produit/sse", produces = "text/event-stream;charset=UTF-8")
    public Flux<Produits> getStream() {
        log.info("Start listening to the course collection.");
        return this.events.map(event -> {
            return (Produits) event.getSource();
        });
    }
    
    @CrossOrigin()
    @GetMapping(value = "/produit", produces = "application/json;charset=UTF-8")
    public List<Produits> get() {
        return produitService.allProduits();
    }
}
