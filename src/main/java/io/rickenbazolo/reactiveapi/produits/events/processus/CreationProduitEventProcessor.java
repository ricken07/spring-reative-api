/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.rickenbazolo.reactiveapi.produits.events.processus;

import io.rickenbazolo.reactiveapi.produits.events.CreationProduit;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Consumer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;
import reactor.core.publisher.FluxSink;

/**
 *
 * @author Ricken BAZOLO
 */
@Slf4j
@Component
public class CreationProduitEventProcessor 
        implements ApplicationListener<CreationProduit>,
        Consumer<FluxSink<CreationProduit>>{
    
    private final Executor executor;
    private final BlockingQueue<CreationProduit> queue = new LinkedBlockingQueue<>();

    public CreationProduitEventProcessor(Executor executor) {
        this.executor = executor;
    }

    @Override
    public void onApplicationEvent(CreationProduit arg0) {
        this.queue.offer(arg0);
    }

    @Override
    public void accept(FluxSink<CreationProduit> arg0) {
        this.executor.execute(() -> {
            while (true) {                
                try {
                    CreationProduit event = queue.take();
                    arg0.next(event);
                }
                catch (InterruptedException e) {
                    ReflectionUtils.rethrowRuntimeException(e);
                }
            }
        });
    }
    
}
