/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.rickenbazolo.reactiveapi.produits.events;

import io.rickenbazolo.reactiveapi.produits.Produits;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

/**
 *
 * @author Ricken BAZOLO
 */
@Getter
public class CreationProduit extends ApplicationEvent{

    public CreationProduit(Produits produits) {
        super(produits);
    }
    
}
