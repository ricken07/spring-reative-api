/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.rickenbazolo.reactiveapi.produits;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Ricken BAZOLO
 */
@Entity
@Data
@NoArgsConstructor
public class Categorie implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull
    private String title;
    @OneToMany(mappedBy = "categorie", targetEntity = Produits.class, 
            fetch = FetchType.LAZY)
    private Set<Produits> produits;

    public Categorie(long id, String title) {
        this.id = id;
        this.title = title;
    }
    
}
